"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.registerEventHtmlGame = void 0;
const eventType_1 = require("./constants/eventType");
const datas_1 = require("./mocks/datas");
const models_1 = require("./models");
const dataCreate = {
    gameSessionId: datas_1.MOCK_GAME_SESSION_ID,
    events: JSON.stringify([{ type: eventType_1.EVENT_TYPE.PING }]),
};
/**
 * Update event
 * @param {number} startTime - Time start register event
 * @returns
 */
const updateGameSessionEvents = (startTime) => {
    const registerEvents = new models_1.RegisterEvents({ startTime });
    registerEvents.register();
    //Update event every 5s
    const requestId = setInterval(async () => {
        await models_1.RegisterEvents.updateGameSessionEvents({
            gameSessionId: datas_1.MOCK_GAME_SESSION_ID,
            events: JSON.stringify(registerEvents.gameSessionEvents),
        });
        registerEvents.setDefault();
    }, 5000);
    return () => {
        clearInterval(requestId);
    };
};
/**
 * Create then update event
 * @param startTime
 */
const registerEventHtmlGame = async () => {
    const startTime = Date.now();
    await models_1.RegisterEvents.createGameSessionEvents(dataCreate);
    updateGameSessionEvents(startTime);
};
exports.registerEventHtmlGame = registerEventHtmlGame;
