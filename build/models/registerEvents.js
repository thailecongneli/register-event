"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RegisterEvents = void 0;
const eventType_1 = require("../constants/eventType");
const axios_1 = __importDefault(require("axios"));
class RegisterEvents {
    constructor({ startTime }) {
        this.gameSessionEvents = [];
        this.startTime = 0;
        this.setDefault = () => {
            console.log("[Register events] - set default");
            this.gameSessionEvents = [{ type: eventType_1.EVENT_TYPE.PING }];
        };
        this.initialize = (startTime) => {
            this.setDefault();
            this.startTime = startTime;
            this.gameSessionEvents = [{ type: eventType_1.EVENT_TYPE.PING }];
        };
        /**
         * Register all event
         * @returns
         */
        this.register = () => {
            const htmlGameCanvas = document.getElementById("UT_CANVAS");
            let isMouseDown = false;
            const mousedownCallback = (event) => {
                isMouseDown = true;
                this.saveMouseEvent(event);
            };
            const mouseupCallback = (event) => {
                this.saveMouseEvent(event);
                isMouseDown = false;
            };
            const mousemoveCallback = (event) => {
                isMouseDown && this.saveMouseEvent(event);
            };
            const touchstartCallback = (event) => {
                this.saveTouchEvent(event);
                isMouseDown = true;
            };
            const touchmoveCallback = (event) => {
                isMouseDown && this.saveTouchEvent(event);
            };
            const touchendCallback = (event) => {
                this.saveTouchEvent(event);
                isMouseDown = false;
            };
            htmlGameCanvas === null || htmlGameCanvas === void 0 ? void 0 : htmlGameCanvas.addEventListener("mousedown", mousedownCallback);
            htmlGameCanvas === null || htmlGameCanvas === void 0 ? void 0 : htmlGameCanvas.addEventListener("mouseup", mouseupCallback);
            htmlGameCanvas === null || htmlGameCanvas === void 0 ? void 0 : htmlGameCanvas.addEventListener("mousemove", mousemoveCallback);
            htmlGameCanvas === null || htmlGameCanvas === void 0 ? void 0 : htmlGameCanvas.addEventListener("touchstart", touchstartCallback);
            htmlGameCanvas === null || htmlGameCanvas === void 0 ? void 0 : htmlGameCanvas.addEventListener("touchmove", touchmoveCallback);
            htmlGameCanvas === null || htmlGameCanvas === void 0 ? void 0 : htmlGameCanvas.addEventListener("touchend", touchendCallback);
            return () => {
                htmlGameCanvas === null || htmlGameCanvas === void 0 ? void 0 : htmlGameCanvas.removeEventListener("mousedown", mousedownCallback);
                htmlGameCanvas === null || htmlGameCanvas === void 0 ? void 0 : htmlGameCanvas.removeEventListener("mouseup", mouseupCallback);
                htmlGameCanvas === null || htmlGameCanvas === void 0 ? void 0 : htmlGameCanvas.removeEventListener("mousemove", mousemoveCallback);
                htmlGameCanvas === null || htmlGameCanvas === void 0 ? void 0 : htmlGameCanvas.removeEventListener("touchstart", touchstartCallback);
                htmlGameCanvas === null || htmlGameCanvas === void 0 ? void 0 : htmlGameCanvas.removeEventListener("touchmove", touchmoveCallback);
                htmlGameCanvas === null || htmlGameCanvas === void 0 ? void 0 : htmlGameCanvas.removeEventListener("touchend", touchendCallback);
            };
        };
        /**
         * Set prescription follow screen push MouseEvent to gameSessionEvents
         * @param event Mouse event from eventListener
         */
        this.saveMouseEvent = (event) => {
            let screenX = window.innerWidth, screenY = window.innerHeight;
            const canvasRatio = screenX / screenY;
            if (canvasRatio > eventType_1.gameRatio) {
                screenX = window.innerHeight * eventType_1.gameRatio;
            }
            else
                screenY = window.innerWidth / eventType_1.gameRatio;
            const canvasEvent = {
                type: event.type,
                screenX: screenX,
                screenY: screenY,
                clientX: event.clientX - (window.innerWidth - screenX) / 2,
                clientY: event.clientY - (window.innerHeight - screenY) / 2,
            };
            const time = Date.now() - this.startTime;
            this.gameSessionEvents.push({
                type: eventType_1.EVENT_TYPE.INPUT,
                time: time,
                data: JSON.stringify(canvasEvent),
            });
        };
        /**
         * Save touch Event
         * @param event - Touch event from eventListener
         */
        this.saveTouchEvent = (event) => {
            const touch = event.changedTouches[0];
            const touchEvent = {
                type: event.type,
                clientX: touch.clientX,
                clientY: touch.clientY,
                screenX: touch.screenX,
                screenY: touch.screenY,
            };
            this.saveMouseEvent(touchEvent);
        };
        this.initialize(startTime);
    }
    /**
     *
     * @param {Object} payload - Object GameSessionId + first event
     * @returns
     */
    static async createGameSessionEvents(payload) {
        try {
            const fullURL = `${this.ROOT_URL}/create`;
            return axios_1.default.post(fullURL, payload);
        }
        catch (e) {
            console.log(e);
        }
    }
    /**
     *
     * @param {Object} payload - Object GameSessionId + list event to update
     * @returns
     */
    static async updateGameSessionEvents(payload) {
        try {
            const fullURL = `${this.ROOT_URL}/update/`;
            return axios_1.default.put(fullURL, payload);
        }
        catch (e) {
            console.log(e);
        }
    }
}
exports.RegisterEvents = RegisterEvents;
RegisterEvents.ROOT_URL = "http://localhost:4100";
