"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.gameRatio = exports.EVENT_TYPE = void 0;
/* eslint-disable no-unused-vars */
var EVENT_TYPE;
(function (EVENT_TYPE) {
    EVENT_TYPE["PING"] = "PING";
    EVENT_TYPE["INPUT"] = "INPUT";
    EVENT_TYPE["PAUSE"] = "PAUSE";
    EVENT_TYPE["RESUME"] = "RESUME";
    EVENT_TYPE["PLAYER_LFET"] = "PLAYER_LFET";
})(EVENT_TYPE = exports.EVENT_TYPE || (exports.EVENT_TYPE = {}));
exports.gameRatio = 444 / 789;
