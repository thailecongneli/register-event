import { EVENT_TYPE } from "./constants/eventType";
import { MOCK_GAME_SESSION_ID } from "./mocks/datas";
import { RegisterEvents } from "./models";

const dataCreate = {
  gameSessionId: MOCK_GAME_SESSION_ID,
  events: JSON.stringify([{ type: EVENT_TYPE.PING }]),
};

/**
 * Update event
 * @param {number} startTime - Time start register event
 * @returns
 */
const updateGameSessionEvents = (startTime: number) => {
  const registerEvents = new RegisterEvents({ startTime });
  registerEvents.register();

  //Update event every 5s
  const requestId = setInterval(async () => {
    await RegisterEvents.updateGameSessionEvents({
      gameSessionId: MOCK_GAME_SESSION_ID,
      events: JSON.stringify(registerEvents.gameSessionEvents),
    });
    registerEvents.setDefault();
  }, 5000);

  return () => {
    clearInterval(requestId);
  };
};

/**
 * Create then update event
 * @param startTime
 */
export const registerEventHtmlGame = async () => {
  const startTime = Date.now();
  await RegisterEvents.createGameSessionEvents(dataCreate);
  updateGameSessionEvents(startTime);
};
